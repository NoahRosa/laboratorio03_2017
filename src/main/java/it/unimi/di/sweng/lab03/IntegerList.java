package it.unimi.di.sweng.lab03;
import java.util.Scanner;
import java.util.function.IntPredicate;
public class IntegerList {
	
	private IntegerNode head= null;
	private IntegerNode tail=null;
	
	public IntegerList(String stringValues){
		Scanner parser = new Scanner(stringValues);
		while (parser.hasNext()){
			String token = parser.next();
			addLast(Integer.parseInt(token));
		}
	}

	public IntegerList() {
		// TODO Auto-generated constructor stub
	}

	public String toString(){
		StringBuilder result= new StringBuilder("[");
		IntegerNode currentNode = head;
		int i =0;
		while(currentNode != null){
			if(i++ >0)
				result.append(" ");
			result.append(currentNode.getValue()); 
			currentNode = currentNode.next();
		}
		return result.append("]").toString();
	}
	
	private void firstNode(int value) {
		head = tail = new IntegerNode(value);
	}
	
	
	public void addLast(int value) {
		if(head == null){
			firstNode(value);
		}
		else{
			IntegerNode node = new IntegerNode(value);
			tail.setNext(node);
			node.setPrevious(tail);
			tail = node;
			}
	}

	

	public void addFirst(int value) {
		if(head == null){
			firstNode(value);
		}
		else{
			IntegerNode node = new IntegerNode(value);
			node.setNext(head);
			head.setPrevious(node);
			head = node;
		}
	}

	public boolean removeFirst() {
		if (head!= null){
			head = head.next();
			if (head == null)
				tail=null;
			return true;
		}
		
		return false;
	}

	public boolean removeLast() {
		if (tail!= null){
			tail = tail.previous();
			if (tail!= null)
				tail.setNext(null);
			else
				head=null;
			return true;
		}
		
		return false;
		
	}

	public boolean remove(int value) {
		IntegerNode currentNode = head;
		while (currentNode != null){
			if (currentNode.getValue()==value){
				currentNode.previous().setNext(currentNode.next());
				currentNode.next().setPrevious(currentNode.previous());
				return true;
			}
		currentNode = currentNode.next();
		}
		return false;
	}

}
