package it.unimi.di.sweng.lab03;

import static org.assertj.core.api.Assertions.*;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

public class LinkedListTest {

	//@Rule
    //public Timeout globalTimeout = Timeout.seconds(2);

	private IntegerList list;
	
	@Test
	public void noParametersConstructor() {
		list = new IntegerList();
		assertThat(list.toString()).isEqualTo("[]");
		
		//fail("Not yet implemented.");
	}
	
	@Test
	public void addLastTest() {
		list = new IntegerList();
		list.addLast(1);
		assertThat(list.toString()).isEqualTo("[1]");
		list.addLast(3);
		assertThat(list.toString()).isEqualTo("[1 3]");
	}
	
	@Test
	public void stringConstructor() {
		list = new IntegerList("");
		assertThat(list.toString()).isEqualTo("[]");
		list = new IntegerList("1");
		assertThat(list.toString()).isEqualTo("[1]");
		list = new IntegerList("1 2 3");
		assertThat(list.toString()).isEqualTo("[1 2 3]");
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void stringConstructorFail() {
		list = new IntegerList("1 2 aaa");
	}
	
	@Test
	public void addFirstTest() {
		list = new IntegerList();
		list.addFirst(1);
		assertThat(list.toString()).isEqualTo("[1]");
		list.addFirst(3);
		assertThat(list.toString()).isEqualTo("[3 1]");
	}
	
	@Test
	public void removeFirstTest() {
		list = new IntegerList("1 2");
		list.removeFirst();
		assertThat(list.toString()).isEqualTo("[2]");
		list.removeFirst();
		assertThat(list.toString()).isEqualTo("[]");
		assertThat(list.removeFirst()).isEqualTo(false);
	}
	
	@Test
	public void removeLastTest() {
		list = new IntegerList("7 10");
		list.removeLast();
		assertThat(list.toString()).isEqualTo("[7]");
		list.removeLast();
		assertThat(list.toString()).isEqualTo("[]");
		assertThat(list.removeLast()).isEqualTo(false);
	}
	
	@Test
	public void removeTest() {
		list = new IntegerList("1 2 3 4 3 5");
		assertThat(list.remove(2)).isEqualTo(true);
		assertThat(list.toString()).isEqualTo("[1 3 4 3 5]");
		assertThat(list.remove(3)).isEqualTo(true);
		assertThat(list.toString()).isEqualTo("[1 4 3 5]");
		assertThat(list.remove(6)).isEqualTo(false);
	}
	
}
